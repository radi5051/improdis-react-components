import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { MyButton, MyButtonProps } from './MyButton';
import { action } from '@storybook/addon-actions';

export default {
    title: 'Improdis/MyButton',
    component: MyButton,
    argTypes: {
      backgroundColor: { control: 'color' },
    },
  } as Meta;

const Template: Story<MyButtonProps> = (args) => <MyButton {...args} />

export const Primary = Template.bind({});
Primary.args = {
  text: "Primary",
  onClick: () => action("clicked"),
};
