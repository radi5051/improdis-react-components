import { Button } from '@material-ui/core';
import React from 'react';

export interface MyButtonProps {
    text: string, 
    onClick: () => void,
    backgroundColor?: string
}
 
export const MyButton: React.FC<MyButtonProps> = ( {text, onClick, backgroundColor}) => {
    return ( 
        <Button style={{ backgroundColor: backgroundColor  }} variant="contained" onClick={onClick}>{text}</Button> );
}